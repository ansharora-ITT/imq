import sys
import os
sys.path.append(os.path.abspath(__file__) + "/../../..")
import sqlite3
from imqServer.src.serverExceptions import *

def writeMessagesToFile(message, address):
    fileName = str(address[0]) + " " + str(address[1])
    file = open(fileName, "a+")
    file.write("\n" + message.decode('utf-8'))

def connectToDatabase():
    try:
        databaseConnection = sqlite3.connect('../../IMQ.db')
        return databaseConnection
    except:
        raise DatabaseException()