import sys
import os
sys.path.append(os.path.abspath(__file__) + "/../../..")
import sqlite3
from datetime import datetime
from imqServer.src.serverExceptions import *

class DatabaseOperations:
    def createUserTable(self, connection):
        try:
            connection.execute('''CREATE TABLE USER
            (USER_ID  int,
            USER_NAME text PRIMARY KEY);''')
        except:
            raise DatabaseException()

    def createMessageTable(self, connection):
        try:
            connection.execute('''CREATE TABLE MESSAGE
            (SERIAL_NO    INT,
            TOPIC   TEXT,
            MESSAGE   TEXT,
            TIMESTAMP   TEXT);''')
        except:
            raise DatabaseException()

    def addClientToUserTable(self, connection, userName):
        try:
            userName = str(userName)
            cursor = connection.cursor()
            cursor.execute(''' SELECT MAX(USER_ID) FROM 'USER'​ ''')
            userId = cursor.fetchone()[0] + 1
            cursor.execute(""" INSERT into USER(USER_ID, USER_NAME) VALUES (?,?)""", (userId,userName))
            connection.commit()
        except:
            raise DatabaseException()

    def addMessageToMessageTable(self, connection, topic, message, timestamp):
        try:
            cursor = connection.cursor()
            if (not self.__isTablePresent("MESSAGE", cursor)):
                self.createMessageTable(connection)
                serialNumber = 1
            else:
                cursor.execute(''' SELECT MAX(SERIAL_NO) FROM 'MESSAGE'​ ''')
                serialNumber = cursor.fetchone()[0] + 1

            cursor.execute(""" INSERT into MESSAGE(SERIAL_NO, TOPIC, MESSAGE, TIMESTAMP) VALUES (?,?,?,?)""", (serialNumber, topic, message, timestamp))
            connection.commit()
        except:
            raise DatabaseException()

    def __isTablePresent(self, tableName, cursor):
        try:
            cursor.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name=(?) ''', (tableName,))
            if (cursor.fetchone()[0] != 1):
                return False
            return True
        except:
            raise DatabaseException()

    def getUsers(self, connection):
        try:
            cursor = connection.cursor()
            if (not self.__isTablePresent("USER", cursor)):
                self.createUserTable(connection)
            cursor.execute(''' SELECT USER_NAME FROM USER ''')
            return cursor.fetchall()
        except:
            raise DatabaseException()

    def getTopics(self, connection):
        try:
            cursor = connection.cursor()
            cursor.execute(''' SELECT TOPIC_NAME FROM TOPIC ''')
            return cursor.fetchall()
        except:
            raise DatabaseException()