from common.src.imqProtocol import Protocol
from enum import Enum

class Response(Protocol):
    responseType: str

class ResponseType(Enum):
    SUCCESS = "SUCCESS"
    FAILED = "FAILED"