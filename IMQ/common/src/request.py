from common.src.imqProtocol import Protocol
from enum import Enum

class Request(Protocol):
    requestType: str

class RequestType(Enum):
    CONNECT_USER  = "CONNECT_USER"
    PUSH_MESSAGE  = "PUSH_MESSAGE"
    PULL_MESSAGE  = "PULL_MESSAGE"
    REGISTER_USER = "REGISTER_USER"
    SHOW_TOPICS   = "SHOW_TOPICS"

