from common.src.request import Request
from common.src.response import Response
from common.src.protocolParser import Parser
from common.src.request import RequestType

class Utility:

    def createRequest(self, data, requestType):
        request = Request()
        request.data = data
        request.requestType = requestType.value
        return request

    def createResponse(self, data, responseType):
        response = Response()
        response.data = data
        response.responseType = responseType.value
        return response
