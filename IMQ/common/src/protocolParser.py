import json

class Parser:
    def getJsonEncoded(header):
        encodedData = json.dumps(header.__dict__)
        return encodedData

    def getJsonDecoded(header):
        return json.loads(header)
