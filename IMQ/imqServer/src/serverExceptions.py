class ServerExceptions(Exception):
    def description(self):
        pass

class InvalidArgumentException(ServerExceptions):
    message = "Argument not valid"

    def description(self):
        print(self.message)

class DatabaseException(ServerExceptions):
    message = "Database unavailable"

    def description(self):
        print(self.message)

class DataHandlerException(DatabaseException):
    message = "Cannot connect to Database"

    def description(self):
        print(self.message)

class SocketException(ServerExceptions):
    message = "Socket not available"

    def description(self):
        print(self.message)
