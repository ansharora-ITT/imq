import sys
import os
sys.path.append(os.path.abspath(__file__) + "/../../..")
from _thread import *
from configparser import ConfigParser
import imqDatabase.src.MessageStorage
import socket
from common.src.protocolParser import Parser
from serverOperations import ServerOperations
from imqServer.src.serverExceptions import *

class Server:

    def __init__(self):
        self.clientCount = 0
        self.configuration = ConfigParser()

    def bindServer(self):
        serverSocket = socket.socket()
        self.configuration.read('../../configuration.ini')
        try:
            serverSocket.bind((self.configuration.get('connectionDetails','host'), int(self.configuration.get('connectionDetails','port'))))
        except:
            raise SocketException()
        return serverSocket

    def connectToClient(self, serverSocket):
        while True:
            client, address = serverSocket.accept()

            print('Connected to client at :' + address[0] + ':' + str(address[1]))
            start_new_thread(self.__clientThread, (client, address))
            self.clientCount += 1
            print('Client Number: ' + str(self.clientCount))

    def __clientThread(self, connection, address):
        try:
            connection.send(str.encode('** Welcome to the server **\n'))
            serverOperations = ServerOperations()
            while True:
                request = connection.recv(int(self.configuration.get('messageSize','messageSize'))).decode('utf-8')
                if request.lower() == "exit":
                    print('** Client at: '+ address[0] + ":" + str(address[1]) + ' disconnected **')
                    break
                try:
                    serverOperations.handleRequest(Parser.getJsonDecoded(request), connection)
                except ServerExceptions as error:
                    error.description()
        except:
            print("Client exited")
            connection.close()