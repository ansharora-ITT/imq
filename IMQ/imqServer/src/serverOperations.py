from common.src.request import RequestType
from common.src.response import Response
from common.src.response import ResponseType
from imqServer.src.dataHandler import DataHandler
from common.src.utility import Utility
from common.src.protocolParser import Parser
from imqServer.src.serverExceptions import *

class ServerOperations:
    def __init__(self):
        self.__dataHandler = DataHandler()

    def handleRequest(self, data, clientSocket):
        if (data["requestType"] == RequestType.CONNECT_USER.value):
            self.connectUser(data, clientSocket)
        elif (data["requestType"] == RequestType.PUSH_MESSAGE.value):
            self.pushMessage(data, clientSocket)
        elif (data["requestType"] == RequestType.REGISTER_USER.value):
            self.registerUser(data, clientSocket)
        elif (data["requestType"] == RequestType.SHOW_TOPICS.value):
            self.showTopics(data, clientSocket)
        elif (data["requestType"] == RequestType.PULL_MESSAGE.value):
            self.pullMessage(data, clientSocket)
        else:
            raise InvalidArgumentException()

    def connectUser(self, data, clientSocket):
        existingUsers = self.__dataHandler.getUsers()
        self.__data = {}
        self.__data["user"] = data["data"]["user"]
        self.__data["requestType"] = data["requestType"]
        if (self.__data["user"] in existingUsers):
            response = self.__createResponse(ResponseType.SUCCESS)
        else:
            self.__data["FailureDetails"] = "User does not exist"
            response = self.__createResponse(ResponseType.FAILED)
        clientSocket.send(str.encode(response))

    def registerUser(self, data, clientSocket):
        existingUsers = self.__dataHandler.getUsers()
        self.__data = {}
        self.__data["user"] = data["data"]["user"]
        self.__data["requestType"] = data["requestType"]
        if (self.__data["user"] in existingUsers):
            self.__data["FailureDetails"] = "User already registered"
            response = self.__createResponse(ResponseType.FAILED)
        else:
            response = self.__createResponse(ResponseType.SUCCESS)
            self.__dataHandler.addUser(self.__data["user"])
        clientSocket.send(str.encode(response))

    def showTopics(self, data, clientSocket):
        existingTopics = self.__dataHandler.getTopics()
        self.__data = {}
        self.__data["requestType"] = data["requestType"]
        if (len(existingTopics) > 0):
            self.__data["topics"] = existingTopics
            response = self.__createResponse(ResponseType.SUCCESS)
        else:
            self.__data["topics"] = []
            self.__data["FailureDetails"] = "No topics available"
            response = self.__createResponse(ResponseType.FAILED)
        clientSocket.send(str.encode(response))

    def pushMessage(self, data, clientSocket):
        self.__data = {}
        self.__data["topic"] = data["data"]["topic"]
        self.__data["requestType"] = data["requestType"]
        existingTopics = self.__dataHandler.getTopics()
        if (self.__data["topic"] in existingTopics):
            response = self.__createResponse(ResponseType.SUCCESS)
            self.__dataHandler.addMessage(data["data"]["topic"], data["data"]["message"], data["data"]["createTime"])
        else:
            self.__data["FailureDetails"] = "Topic not available"
            response = self.__createResponse(ResponseType.FAILED)
        clientSocket.send(str.encode(response))

    def pullMessage(self, data, clientSocket):
        self.__data = {}
        self.__data["topic"] = data["data"]["topic"]
        self.__data["requestType"] = data["requestType"]
        existingTopics = self.__dataHandler.getTopics()
        if (self.__data["topic"] in existingTopics):
            self.__data["messages"] = self.__dataHandler.getMessages(self.__data["topic"])
            response = self.__createResponse(ResponseType.SUCCESS)
        else:
            self.__data["FailureDetails"] = "Topic not available"
            response = self.__createResponse(ResponseType.FAILED)
        clientSocket.send(str.encode(response))

    def __createResponse(self, responseType):
        utility = Utility()
        response = utility.createResponse(self.__data, responseType)
        jsonEncoded = Parser.getJsonEncoded(response)
        return jsonEncoded
