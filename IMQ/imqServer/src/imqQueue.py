class ImqQueue:

    def __init__(self, topicName):
        self.__topicName = topicName
        self.__messages = []

    def getTopicName(self):
        return self.__topicName

    def pushMessage(self, message):
        self.__messages.append(message)

    def pullMessages(self):
        return self.__messages