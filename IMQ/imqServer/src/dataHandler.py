from imqDatabase.src.DatabaseOperations import DatabaseOperations
from imqDatabase.src.MessageStorage import *
from imqServer.src.imqQueue import ImqQueue
from imqServer.src.serverExceptions import *

class Message:
    __message: str
    __createTime: str

    def setMessageData(self, message, createTime):
        self.__message = message
        self.__createTime = createTime

    def getMessage(self):
        return self.__message

class DataHandler:
    __queueList = []
    __users = []
    __topics = []

    def __init__(self):
        try:
            self.databaseOperations = DatabaseOperations()
            self.databaseConnection = connectToDatabase()
            self.__loadUsers()
            self.__loadTopics()
            self.__createQueues()
        except:
            raise DataHandlerException()

    def __loadTopics(self):
        topicRecords = self.databaseOperations.getTopics(self.databaseConnection)
        for topic in topicRecords:
            self.__topics.append(topic[0])

    def __loadUsers(self):
        userRecords = self.databaseOperations.getUsers(self.databaseConnection)
        for user in userRecords:
            self.__users.append(user[0])

    def __createQueues(self):
        for topic in self.__topics:
            queue = ImqQueue(topic)
            self.__queueList.append(queue)

    def addUser(self, userName):
        self.__users.append(userName)
        self.databaseOperations.addClientToUserTable(self.databaseConnection, userName)

    def addMessage(self, topic, message, createTime):
        messageObj = Message()
        messageObj.setMessageData(message, createTime)
        for queue in self.__queueList:
            if queue.getTopicName() == topic:
                queue.pushMessage(messageObj)
                self.databaseOperations.addMessageToMessageTable(self.databaseConnection, topic, message, createTime)
                break

    def getMessages(self, topic):
        messageObjects = []
        messages = []
        for queue in self.__queueList:
            if queue.getTopicName() == topic:
                messageObjects = queue.pullMessages()
                break
        for messageObj in messageObjects:
            messages.append(messageObj.getMessage())
        return messages

    def getUsers(self):
        return self.__users

    def getTopics(self):
        return self.__topics
