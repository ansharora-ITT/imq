import sys
import os
sys.path.append(os.path.abspath(__file__) + "/../../..")
from _thread import *
from configparser import ConfigParser
import imqDatabase.src.MessageStorage
import socket
from common.src.protocolParser import Parser
from serverOperations import ServerOperations
from imqServer.src.server import Server

def runServer():
    server = Server()
    serverSocket = server.bindServer()
    print('Waiting for Connection..')
    serverSocket.listen(5)
    server.connectToClient(serverSocket)
    serverSocket.close()

runServer()
