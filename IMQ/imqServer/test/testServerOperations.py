import unittest
import sys
import os
import socket
sys.path.append(os.path.abspath(__file__) + "/../../..")
from unittest.mock import Mock, create_autospec, patch
from common.src.response import Response
from common.src.response import ResponseType
from imqServer.src.dataHandler import DataHandler
from imqServer.src.serverOperations import ServerOperations
from common.src.utility import Utility
from common.src.protocolParser import Parser
from common.src.request import RequestType

class TestServerOperations(unittest.TestCase):
    __serverOperations : None

    mockDataHandler = create_autospec(DataHandler)
    mockDataHandler.getUsers.return_value = ["ansh","vansh"]

    mockSocket = create_autospec(socket.socket)
    mockSocket.send.return_value = True

    __data = {"data":{"user":"ansh"}, "requestType":"CONNECT_USER"}
    def test_connectUser_sends_the_response(self):
        self.__serverOperations = ServerOperations()
        self.__serverOperations.__dataHandler = self.mockDataHandler
        self.__serverOperations.connectUser(self.__data, self.mockSocket)
        assert self.mockSocket.send.called

    __dataWithInvalidUser = {"data":{"user":"sample"}, "requestType":"CONNECT_USER"}
    def test_connectUser_sends_the_response_withFailureDetails(self):
        self.__serverOperations = ServerOperations()
        self.__serverOperations.__dataHandler = self.mockDataHandler
        self.__serverOperations.connectUser(self.__dataWithInvalidUser, self.mockSocket)
        assert self.mockSocket.send.called

