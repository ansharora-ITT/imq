import unittest
import sys
import os
sys.path.append(os.path.abspath(__file__) + "/../../..")
from unittest.mock import Mock, create_autospec, patch
from imqClient.src.commandParser import CommandParser

class TestCommandParser(unittest.TestCase):
    __commandParser : None

    #positive scenarios test cases
    def test_validateCommand_for_connect(self):
        self.__commandParser = CommandParser()
        result = self.__commandParser.validateCommand("imq connect topic_name")
        assert True == result
        assert "connect" == self.__commandParser.getAction()
        assert "topic_name" == self.__commandParser.getValue()
        assert "" == self.__commandParser.getMessage()

    def test_validateCommand_for_publish(self):
        self.__commandParser = CommandParser()
        result = self.__commandParser.validateCommand("imq publish topic_name -m \"sample_message\"")
        assert True == result
        assert "publish" == self.__commandParser.getAction()
        assert "topic_name" == self.__commandParser.getValue()
        assert "sample_message" == self.__commandParser.getMessage()

    def test_validateCommand_for_show(self):
        self.__commandParser = CommandParser()
        result = self.__commandParser.validateCommand("imq show topics")
        assert True == result
        assert "show" == self.__commandParser.getAction()
        assert "topics" == self.__commandParser.getValue()
        assert "" == self.__commandParser.getMessage()

    def test_validateCommand_for_subscribe(self):
        self.__commandParser = CommandParser()
        result = self.__commandParser.validateCommand("imq subscribe topic_name")
        assert True == result
        assert "subscribe" == self.__commandParser.getAction()
        assert "topic_name" == self.__commandParser.getValue()
        assert "" == self.__commandParser.getMessage()

    def test_validateCommand_for_incorrect_keyword(self):
        self.__commandParser = CommandParser()
        result = self.__commandParser.validateCommand("imq config")
        assert False == result

    def test_validateCommand_for_incorrect_flag(self):
        self.__commandParser = CommandParser()
        result = self.__commandParser.validateCommand("imq publish topic_name -k \"sample_message\"")
        assert False == result
