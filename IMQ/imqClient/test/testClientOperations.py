import unittest
import sys
import os
sys.path.append(os.path.abspath(__file__) + "/../../..")
from unittest.mock import Mock, create_autospec, patch
from imqClient.src.clientOperations import ClientOperations
from common.src.protocolParser import Parser
from common.src.request import RequestType

class TestClientOperations(unittest.TestCase):
    __clientOperations : None

    def test_createRequest_creates_valid_request(self):
        self.__clientOperations = ClientOperations()
        result = self.__clientOperations.pushMessage("hello","topic_name","2:31",RequestType.PUSH_MESSAGE)
        result = Parser.getJsonDecoded(result)
        assert "hello" == result["data"]["message"]
        assert "topic_name" == result["data"]["topic"]
        assert "2:31" == result["data"]["createTime"]
        assert RequestType.PUSH_MESSAGE.value == result["requestType"]
