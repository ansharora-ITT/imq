class ClientExceptions(Exception):
    def description(self):
        pass

class InvalidCommandException(ClientExceptions):
    message = "Command not valid"

    def description(self):
        print(self.message)

class ServerException(ClientExceptions):
    message = "Server not available"

    def description(self):
        print(self.message)

class SocketException(ServerException):
    message = "Socket not available"

    def description(self):
        print(self.message)


