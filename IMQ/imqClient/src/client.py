import sys
import os
sys.path.append(os.path.abspath(__file__) + "/../../..")
from configparser import ConfigParser
import socket
from common.src.protocolParser import Parser
from common.src.request import Request
from common.src.request import RequestType
from imqClient.src.commandParser import CommandParser
from imqClient.src.clientOperations import ClientOperations
from datetime import datetime
from imqClient.src.clientExceptions import *
class Client:
    __configuration = ConfigParser()
    __isConnected = False

    def connectToServer(self):
        clientSocket = socket.socket()
        self.__configuration.read('../../configuration.ini')
        try:
            clientSocket.connect((self.__configuration.get('connectionDetails','host'), int(self.__configuration.get('connectionDetails','port'))))

        except:
            raise SocketException()

        return clientSocket

    def __receiveMessage(self, clientSocket):
        try:
            response = clientSocket.recv(int(self.__configuration.get('messageSize','messageSize')))
            return response
        except:
            raise ServerException()

    def talkToServer(self, clientSocket):
        welcomeMessage = self.__receiveMessage(clientSocket)
        print(welcomeMessage.decode('utf-8'))
        while True:
            command = input("<<<< ")
            if command.lower() == "exit":
                clientSocket.send(str.encode(command))
                break
            try:
                action,value,message = self.__getCommandValues(command)
                request = self.__createRequest(action,value,message)
                clientSocket.send(str.encode(request))
                response = self.__receiveMessage(clientSocket).decode('utf-8')
                if (not self.__isConnected):
                    self.__setIsConnected(Parser.getJsonDecoded(response))
                print(">>>> " + response)
            except ClientExceptions as error:
                error.description()

        clientSocket.close()

    def __getCommandValues(self, command):
        commandParser = CommandParser()
        if (commandParser.validateCommand(command)):
            action = commandParser.getAction()
            value = commandParser.getValue()
            message = commandParser.getMessage()
            return action,value,message
        else:
            raise InvalidCommandException()

    def __createRequest(self, action, value, message):
        clientOperations = ClientOperations()
        request = ""
        if (action == "register"):
            request = clientOperations.registerUser(value, RequestType.REGISTER_USER)
        elif (action == "connect"):
            if ((not self.__isConnected)):
                request = clientOperations.connectUser(value, RequestType.CONNECT_USER)
        elif (action == "show"):
            request = clientOperations.showTopics(RequestType.SHOW_TOPICS)
        elif (action == "publish"):
            request = clientOperations.pushMessage(message, value, str(datetime.now()), RequestType.PUSH_MESSAGE)
        elif (action == "subscribe"):
            request = clientOperations.pullMessage(value, RequestType.PULL_MESSAGE)
        return request

    def __setIsConnected(self, response):
        if (response["data"]["requestType"] == "connect_user" and response["responseType"] == "success"):
            self.__isConnected = True