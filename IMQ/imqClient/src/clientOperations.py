from common.src.utility import Utility
from common.src.request import RequestType
from common.src.protocolParser import Parser

class ClientOperations:
    def __init__(self):
        self.data = {}

    def pushMessage(self, message, topic, createTime, requestType):
        self.data["topic"] = topic
        self.data["message"] = message
        self.data["createTime"] = createTime
        request = self.__createRequest(requestType)
        return request

    def pullMessage(self, topic, requestType):
        self.data["topic"] = topic
        request = self.__createRequest(requestType)
        return request

    def connectUser(self, userName, requestType):
        self.data["user"] = userName
        request = self.__createRequest(requestType)
        return request

    def registerUser(self, userName, requestType):
        self.data["user"] = userName
        request = self.__createRequest(requestType)
        return request

    def showTopics(self, requestType):
        request = self.__createRequest(requestType)
        return request

    def __createRequest(self, requestType):
        utility = Utility()
        request = utility.createRequest(self.data, requestType)
        jsonEncoded = Parser.getJsonEncoded(request)
        return jsonEncoded
