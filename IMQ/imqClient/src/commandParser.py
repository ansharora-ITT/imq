import re

class CommandParser:
    def __init__(self):
        self.keywords = ["publish", "connect", "subscribe", "show", "register"]
        self.action : str
        self.value : ""
        self.message = ""

    def __validateKeywords(self, command):
        inputKeywords = command.split(" ")
        validKeyword = False
        if (inputKeywords[0] == "imq" and (inputKeywords[1] in self.keywords)):
            self.action = inputKeywords[1]
            self.value = inputKeywords[2]
            validKeyword = True
        return validKeyword

    def __validateFlag(self, command):
        validFlag = False
        regex = self.__buildRegex()
        if(re.search(regex, command)):
            validFlag = True
        return validFlag

    def __buildRegex(self):
        regex = "imq " + self.action + " " + self.value + " " + r'-m ["][\w\s]+["]'
        return regex

    def validateCommand(self, command):
        validCommand = False
        if (self.__validateKeywords(command)):
            if (self.action == "publish"):
                if (self.__validateFlag(command)):
                    self.__extractMessage(command)
                    validCommand = True
            elif ((self.action == "connect") and (len(command.split(" ")) == 3)):
                validCommand = True
            elif ((self.action == "subscribe") and (len(command.split(" ")) == 3)):
                validCommand = True
            elif ((self.action == "show") and (self.value == "topics") and (len(command.split(" ")) == 3)):
                validCommand = True
            elif ((self.action == "register") and (len(command.split(" ")) == 3)):
                validCommand = True
        else :
            validCommand = False
        return validCommand

    def getAction(self):
        return self.action

    def getValue(self):
        return self.value

    def getMessage(self):
        return self.message

    def __extractMessage(self, command):
        regex = r'["][\w\s]+["]'
        match = re.findall(regex, command)
        self.message = match[0][1:len(match[0])-1]
