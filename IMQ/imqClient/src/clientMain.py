import sys
import os
sys.path.append(os.path.abspath(__file__) + "/../../..")
from configparser import ConfigParser
import socket
from common.src.protocolParser import Parser
from common.src.request import Request
from common.src.request import RequestType
from imqClient.src.client import Client

def runClient():
    try:
        newClient = Client()
        print('Connecting To Server....')
        try:
            clientSocket = newClient.connectToServer()
            newClient.talkToServer(clientSocket)
        except ClientExceptions as error:
            error.description()
    except:
        print("Client Closed")
runClient()